# flutter_simple_test


## Requirements

- [Flutter 2.5.0+ (stable channel)](https://flutter.dev/docs/get-started/install)
- [Dart 2.13.4+](https://github.com/dart-lang/sdk/wiki/Installing-beta-and-dev-releases-with-brew,-choco,-and-apt-get#installing)

## Environment

**iOS**

- iOS 13+

**Android**

- Android 5.1+
    - minSdkVersion 16
- targetSdkVersion 30

![](https://bloclibrary.dev/assets/bloc_architecture_full.png)

## App architecture

- Based on [BLoC (Business Logic Component)](https://bloclibrary.dev/#/architecture)

## Code Style
- [Effective Dart](https://dart.dev/guides/language/effective-dart)

## Dependencies
-  [flutter_bloc](https://pub.dev/packages/flutter_bloc)
-  [bloc_test](https://pub.dev/packages/bloc_test)

## Getting Started
### Setup

```
$ flutter pub get
```