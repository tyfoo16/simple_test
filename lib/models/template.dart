import 'product.dart';

class Template {
  final int id;
  final String title;
  final String notes;
  final List<Product> products;

  Template(
    this.id,
    this.title,
    this.notes,
    this.products,
  );

  copyWith({
    int? id,
    String? title,
    String? notes,
    List<Product>? products,
  }) {
    return Template(
      id ?? this.id,
      title ?? this.title,
      notes ?? this.notes,
      products ?? this.products,
    );
  }
}
