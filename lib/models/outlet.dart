class Outlet {
  final String id;
  final String value;

  Outlet(this.id, this.value);
}
