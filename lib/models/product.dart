class Product {
  final String title;
  final String sku;
  final int quantity;
  final String uom;
  final double estUnitPrice;
  final double estTotalPrice;
  final String notes;

  Product(
    this.title,
    this.sku,
    this.quantity,
    this.uom,
    this.estUnitPrice,
    this.estTotalPrice,
    this.notes,
  );

  copyWith({
    title,
    sku,
    quantity,
    uom,
    estUnitPrice,
    estTotalPrice,
    notes,
  }) {
    return Product(
      title ?? this.title,
      sku ?? this.sku,
      quantity ?? this.quantity,
      uom ?? this.uom,
      estUnitPrice ?? this.estUnitPrice,
      estTotalPrice ?? this.estTotalPrice,
      notes ?? this.notes,
    );
  }
}
