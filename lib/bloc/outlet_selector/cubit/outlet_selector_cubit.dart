import 'package:bloc/bloc.dart';
import 'package:flutter_simple_test/models/outlet.dart';
import 'package:meta/meta.dart';

part 'outlet_selector_state.dart';

class OutletSelectorCubit extends Cubit<OutletSelectorState> {
  OutletSelectorCubit() : super(OutletSelectorInitial());

  void outletSelected(Outlet outlet) {
    emit(OutletSelectorUpdated(selected: outlet));
  }
}
