part of 'outlet_selector_cubit.dart';

@immutable
abstract class OutletSelectorState {}

class OutletSelectorInitial extends OutletSelectorState {}

class OutletSelectorUpdated extends OutletSelectorState {
  final Outlet selected;
  OutletSelectorUpdated({
    required this.selected,
  });
}
