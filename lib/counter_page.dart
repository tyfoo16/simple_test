import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_simple_test/UI/screens/template_screen.dart';
import 'package:flutter_simple_test/bloc/counter/cubit/counter_cubit.dart';
import 'package:flutter_simple_test/bloc/outlet_selector/cubit/outlet_selector_cubit.dart';

import 'package:flutter_simple_test/design_page.dart';

class CounterPage extends StatefulWidget {
  CounterPage({Key? key, required this.title}) : super(key: key);

  final String title;

  @override
  _CounterPageState createState() => _CounterPageState();
}

class _CounterPageState extends State<CounterPage> {
  void _incrementCounter() {
    BlocProvider.of<CounterCubit>(context).increaseCounter();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
        actions: <Widget>[
          PopupMenuButton<String>(
            onSelected: menuAction,
            itemBuilder: (BuildContext context) {
              return {'Show Design', 'Build UI'}.map((String choice) {
                return PopupMenuItem<String>(
                  value: choice,
                  child: Text(choice),
                );
              }).toList();
            },
          ),
        ],
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            BlocBuilder<CounterCubit, int>(
              builder: (context, state) {
                return Text(state.toString());
              },
            ),
          ],
        ),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: _incrementCounter,
        tooltip: 'Increment',
        child: Icon(Icons.add),
      ),
    );
  }

  void menuAction(String value) {
    switch (value) {
      case 'Show Design':
        Navigator.of(context)
            .push(MaterialPageRoute(builder: (context) => DesignPage()));
        break;
      case 'Build UI':
        Navigator.of(context).push(
          MaterialPageRoute(
            builder: (context) => MultiBlocProvider(
              providers: [
                BlocProvider(
                  create: (context) => OutletSelectorCubit(),
                ),
              ],
              child: TemplateScreen(),
            ),
          ),
        );
        break;
      default:
        break;
    }
  }
}
