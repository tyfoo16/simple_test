import 'package:flutter/material.dart';
import 'package:flutter_simple_test/models/outlet.dart';

class BuildUi extends StatelessWidget {
  const BuildUi({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(),
      body: Container(
        child: Column(
          children: [
            _buildTopBar(),
            _buildMainContent(),
          ],
        ),
      ),
    );
  }

  Widget _buildTopBar() {
    return Container(
      padding: EdgeInsets.all(8),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          _buildDropDownButton(),
          ElevatedButton(
            style: ElevatedButton.styleFrom(
              primary: Colors.green.shade800,
              padding: EdgeInsets.symmetric(horizontal: 16, vertical: 2),
              shape: StadiumBorder(),
              minimumSize: Size(90, 20),
            ),
            onPressed: () {},
            child: Text(
              "Template",
              style: TextStyle(
                fontSize: 12,
              ),
            ),
          ),
        ],
      ),
    );
  }

  Widget _buildDropDownButton() {
    final outlets = [
      Outlet("KLCC", "KLCC Outlet"),
      Outlet("SB", "Subang Outlet"),
    ];
    return Container(
      child: DropdownButton(
        underline: SizedBox(),
        value: "KLCC",
        onChanged: (v) {},
        items: outlets
            .map(
              (e) => DropdownMenuItem(
                value: e.id,
                child: Text(
                  e.value,
                  style: TextStyle(color: Colors.black45, fontSize: 14),
                ),
              ),
            )
            .toList(),
      ),
    );
  }

  Widget _buildMainContent() {
    return Expanded(
      child: Container(),
    );
  }
}
