import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_simple_test/bloc/outlet_selector/cubit/outlet_selector_cubit.dart';
import 'package:flutter_simple_test/models/outlet.dart';
import 'package:flutter_simple_test/models/product.dart';

class TemplateScreen extends StatefulWidget {
  TemplateScreen({Key? key}) : super(key: key);

  @override
  _TemplateScreenState createState() => _TemplateScreenState();
}

class _TemplateScreenState extends State<TemplateScreen> {
  final outlets = [
    Outlet("KLCC", "KLCC Outlet"),
    Outlet("SB",
        "Subang Outlet sadsadsadsadsadsadaasdsadsadsadsasasdsadsadsadsadsadsadsadsdsadsaddas"),
  ];

  var products = [
    Product("Chicken", "SKU-2122121", 19900000, "PCS", 99999991.25,
        999999625.00, ""),
    Product("Banana", "SKU-2122121", 10, "PCS", 1.25, 625.00, ""),
    Product("Lamb", "SKU-2122121", 10, "PCS", 1.25, 625.00, ""),
  ];

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onVerticalDragEnd: (details) {
        final _y = details.velocity.pixelsPerSecond.dy;

        if (_y > 500) {
          print("Back");
          Navigator.pop(context);
        }
      },
      child: Scaffold(
        // appBar: AppBar(),
        backgroundColor: CupertinoColors.extraLightBackgroundGray,
        body: SafeArea(
          child: Container(
            child: Column(
              children: [
                _buildTopBar(),
                _buildMainContent(),
                _buildBottomSheet(),
              ],
            ),
          ),
        ),
      ),
    );
  }

  Widget _buildTopBar() {
    return Container(
      padding: EdgeInsets.all(8),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          BackButton(
            color: Colors.black54,
          ),
          _buildDropDownButton(),
          Expanded(child: SizedBox()),
          ElevatedButton(
            style: ElevatedButton.styleFrom(
              primary: Colors.green.shade800,
              padding: EdgeInsets.symmetric(horizontal: 16, vertical: 2),
              shape: StadiumBorder(),
              minimumSize: Size(90, 20),
            ),
            onPressed: () {},
            child: Text(
              "Template",
              style: TextStyle(
                fontSize: 12,
              ),
            ),
          ),
        ],
      ),
    );
  }

  Widget _buildDropDownButton() {
    return BlocBuilder<OutletSelectorCubit, OutletSelectorState>(
      builder: (context, state) {
        var selected = outlets.first;
        if (state is OutletSelectorUpdated) {
          selected = state.selected;
        }
        return Flexible(
          child: Container(
            child: DropdownButton(
              isExpanded: true,
              underline: SizedBox(),
              value: selected,
              onChanged: (Outlet? v) {
                if (v == null) {
                  return;
                }
                BlocProvider.of<OutletSelectorCubit>(context).outletSelected(v);
              },
              items: outlets
                  .map(
                    (e) => DropdownMenuItem(
                      value: e,
                      child: Text(
                        e.value,
                        style: TextStyle(color: Colors.black45, fontSize: 14),
                      ),
                    ),
                  )
                  .toList(),
            ),
          ),
        );
      },
    );
  }

  Widget _buildMainContent() {
    return Expanded(
      child: Container(
        child: Stack(
          children: [
            Padding(
              padding: EdgeInsets.symmetric(horizontal: 8),
              child: Column(
                mainAxisSize: MainAxisSize.max,
                children: [
                  _buildInfo(),
                  SizedBox(height: 16),
                  _buildFilterBar(),
                  SizedBox(height: 16),
                  _buildProductList(),
                ],
              ),
            ),
            Positioned(
              bottom: 8,
              right: 4,
              child: ElevatedButton(
                onPressed: () {},
                child: Icon(Icons.add, size: 30),
                style: ElevatedButton.styleFrom(
                  shape: CircleBorder(),
                  primary: Colors.brown,
                  padding: EdgeInsets.all(8),
                ),
              ),
            )
          ],
        ),
      ),
    );
  }

  Widget _buildInfo() {
    return Container(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          _buildLabel("Quick Order - Chicken Thigh",
              fontSize: 16, fontWeight: FontWeight.bold),
          SizedBox(height: 8),
          _buildLabel(
              "This is an order note. It will  always appear in the orders created from this template. User can still edit this when drafting the order.",
              maxlines: 3,
              color: Colors.black54),
        ],
      ),
    );
  }

  Widget _buildLabel(
    String text, {
    double fontSize = 12,
    FontWeight fontWeight = FontWeight.w500,
    Color color = Colors.black,
    int? maxlines,
    TextAlign textAlign = TextAlign.left,
  }) {
    return Container(
      child: Text(
        text,
        // overflow: TextOverflow.ellipsis,
        textAlign: textAlign,
        maxLines: maxlines,
        style: TextStyle(
          fontSize: fontSize,
          fontWeight: fontWeight,
          color: color,
        ),
      ),
    );
  }

  Widget _buildFilterBar() {
    return Container(
      padding: EdgeInsets.only(top: 8, left: 8, bottom: 8),
      child: Row(
        children: [
          Expanded(
            child: Container(
              padding: EdgeInsets.symmetric(
                horizontal: 4,
              ),
              decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.circular(5),
                boxShadow: [
                  BoxShadow(
                    color: Colors.black12,
                    spreadRadius: 0,
                    blurRadius: 32,
                  ),
                ],
              ),
              child: SizedBox(
                height: 30,
                child: TextFormField(
                  initialValue: "",
                  decoration: InputDecoration(
                    icon: Icon(Icons.search),
                    border: InputBorder.none,
                    hintText: "Filter item",
                    hintStyle: TextStyle(color: Colors.black38, fontSize: 14),
                  ),
                ),
              ),
            ),
          ),
          IconButton(onPressed: () {}, icon: Icon(Icons.filter_list))
        ],
      ),
    );
  }

  Widget _buildProductList() {
    return Expanded(
      child: Container(
        child: ListView.builder(
          itemCount: products.length,
          itemBuilder: (context, i) {
            return _buildProduct(products[i]);
          },
        ),
      ),
    );
  }

  Widget _buildProduct(Product p) {
    return Container(
      child: Card(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(15.0),
        ),
        color: Colors.white,
        child: Container(
          padding: EdgeInsets.all(12),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              _buildLabel(p.title, fontWeight: FontWeight.w700, fontSize: 16),
              SizedBox(height: 8),
              _buildLabel(p.sku, fontSize: 10, color: Colors.black26),
              SizedBox(height: 8),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  _buildSubInfo("Order Qty", "${p.quantity}", highlight: true),
                  _buildSubInfo("UOM", "${p.uom}"),
                  _buildSubInfo(
                    "Est. unit price(RM)",
                    "${p.estUnitPrice}",
                    align: CrossAxisAlignment.end,
                  ),
                  _buildSubInfo(
                    "Est. total(RM)",
                    "${p.estTotalPrice}",
                    helperText: "Including tax &\ndiscounts",
                    align: CrossAxisAlignment.end,
                  ),
                ],
              ),
              SizedBox(height: 8),
              Container(
                child: CupertinoTextField.borderless(
                  style: TextStyle(fontSize: 12, color: Colors.black87),
                  placeholder: "Enter your notes here.",
                  placeholderStyle: TextStyle(
                    fontSize: 12,
                    color: Colors.black26,
                    fontStyle: FontStyle.italic,
                  ),
                ),
              ),
              SizedBox(height: 24),
              Align(
                alignment: Alignment.bottomCenter,
                child: SizedBox(
                  height: 16,
                  child: TextButton(
                    style: TextButton.styleFrom(
                      padding: EdgeInsets.all(0),
                      textStyle: TextStyle(
                        fontSize: 10,
                        decoration: TextDecoration.underline,
                      ),
                    ),
                    onPressed: () {},
                    child: Text(
                      "Show Details",
                    ),
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  Widget _buildSubInfo(
    String title,
    String text, {
    bool highlight = false,
    String? helperText,
    CrossAxisAlignment align = CrossAxisAlignment.start,
  }) {
    return Flexible(
      child: Container(
        child: Column(
          crossAxisAlignment: align,
          children: [
            _buildLabel(title, fontSize: 8, color: Colors.black45),
            SizedBox(height: 4),
            _buildLabel(
              text,
              fontSize: 20,
              color: highlight ? Colors.blue : Colors.black87,
              textAlign: align == CrossAxisAlignment.end
                  ? TextAlign.right
                  : TextAlign.left,
            ),
            SizedBox(height: 4),
            helperText != null
                ? _buildLabel(
                    helperText,
                    fontSize: 8,
                    color: Colors.black45,
                    textAlign: align == CrossAxisAlignment.end
                        ? TextAlign.right
                        : TextAlign.left,
                  )
                : SizedBox(),
          ],
        ),
      ),
    );
  }

  Widget _buildBottomSheet() {
    return Container(
      padding: EdgeInsets.all(8),
      decoration: BoxDecoration(color: Colors.white, boxShadow: [
        BoxShadow(
          blurRadius: 20,
          spreadRadius: 0,
          color: Colors.black12,
        )
      ]),
      child: Column(
        children: [
          SizedBox(
            height: 16,
            child: TextButton(
              style: TextButton.styleFrom(
                padding: EdgeInsets.all(0),
                textStyle: TextStyle(
                  fontSize: 12,
                  decoration: TextDecoration.underline,
                ),
              ),
              onPressed: () {},
              child: Text(
                "Show Details",
              ),
            ),
          ),
          SizedBox(height: 16),
          Row(
            children: [
              Expanded(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    _buildLabel(
                      "Estimated grand total",
                      fontSize: 18,
                      color: Colors.black38,
                      fontWeight: FontWeight.w400,
                    ),
                    _buildLabel(
                      "Including tax & discounts",
                      fontSize: 8,
                      color: Colors.black26,
                      fontWeight: FontWeight.w400,
                    ),
                  ],
                ),
              ),
              Column(
                crossAxisAlignment: CrossAxisAlignment.end,
                children: [
                  _buildLabel(
                    "625.00",
                    fontSize: 20,
                    color: Colors.black87,
                    fontWeight: FontWeight.bold,
                  ),
                  _buildLabel(
                    "Malaysia Ringgit(MYR)",
                    fontSize: 8,
                    color: Colors.black38,
                    fontWeight: FontWeight.w400,
                  ),
                ],
              ),
            ],
          ),
          SizedBox(height: 16),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              ElevatedButton(
                onPressed: () {
                  Navigator.of(context).pop();
                },
                child: Text(
                  "DISCARD TEMPLATE",
                  style: TextStyle(
                      color: Colors.black54, fontWeight: FontWeight.bold),
                ),
                style: ElevatedButton.styleFrom(
                  primary: Colors.white,
                  onPrimary: Colors.redAccent,
                  elevation: 0,
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(10),
                    side: BorderSide(color: Colors.black38, width: 1),
                  ),
                ),
              ),
              ElevatedButton(
                onPressed: () {},
                child: Text(
                  "SAVE TEMPLATE",
                  style: TextStyle(
                      color: Colors.black87, fontWeight: FontWeight.bold),
                ),
                style: ElevatedButton.styleFrom(
                  primary: Colors.amber,
                  elevation: 0,
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(10),
                  ),
                ),
              ),
            ],
          )
        ],
      ),
    );
  }
}
