import 'package:bloc_test/bloc_test.dart';
import 'package:flutter_simple_test/bloc/counter/cubit/counter_cubit.dart';
import 'package:flutter_test/flutter_test.dart';

class MockCounterCubit extends MockCubit<int> implements CounterCubit {}

void main() {
  test("Test return correct state", () async {
    final bloc = MockCounterCubit();
    whenListen(
      bloc,
      Stream.fromIterable([0, 1]),
      initialState: 0,
    );

    expect(bloc.state, equals(0));
    await expectLater(
      bloc.stream,
      emitsInOrder([0, 1]),
    );

    expect(bloc.state, equals(1));
  });

  blocTest(
    'CounterCubit emits [100] inscrease counter twice when seeded with 98',
    build: () => CounterCubit(),
    seed: () => 98,
    act: (CounterCubit cubit) => cubit..increaseCounter()..increaseCounter(),
    skip: 1,
    expect: () => [
      100,
    ],
  );
}
